var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "addpage.html",
            controller : "crud_Controller"
        })
        .when("/table", {
            templateUrl : "tabledata.html",
            controller : "crud_Controller"
        })
});
app.controller('crud_Controller',function ($scope,$location) {
    $scope.EmpModel = {
        Id: 1,
        CompanyName: '',
        ProductName: '',
        Emailid: '',
        CategoryName:  '',
        Datepicker:  ''
    };
    $scope.categorynames = ["Please Select Product Category","Industry", "Functionality", "Customer Needs","Quality"];
    $scope.EmpList = [];
    $('.update-btn').css('display','none');
    $('.cnl-btn').css('display','none');
    $('.edit-form').css('display','none');

    $scope.addData = function () {
        var empObj = {
            /* Id: $scope.EmpList.length + 1,*/
            Id: Date.now(),
            CompanyName: $scope.EmpModel.CompanyName,
            ProductName: $scope.EmpModel.ProductName,
            Emailid: $scope.EmpModel.Emailid,
            CategoryName: $scope.EmpModel.CategoryName,
            DatepickerSelect: $scope.EmpModel.DatepickerSelect
        };

        console.log(' DatepickerSelect. ....//',empObj.DatepickerSelect);
        console.log(' EmpModel. ....//',$scope.EmpList);


        /* $scope.EmpList.push(empObj);
         $.notify("Add Data Successfully","success");*/
        if(empObj.CompanyName && empObj.ProductName !== ""  && empObj.Emailid && empObj.CategoryName !==''){
            $scope.EmpList.push(empObj);
            $.notify("Add Data Successfully","success");
            $scope.clearModel();
            localStorage.setItem("items", JSON.stringify($scope.EmpList));
            /*$location.path("table");*/
            $location.path('/table');
        }else{
            $.notify("Please enter the data","error");
        }
    };
    $scope.addNew = function () {
        $location.url('/');

    };
    $scope.orderByMe = function(Emp) {
        $scope.myOrderBy = Emp;
    };
    $scope.updateData = function () {
        /*  $.grep($scope.EmpList, function (e) {*/
        angular.forEach($scope.EmpList, function (e) {
            if (e.Id == $scope.EmpModel.Id) {
                e.CompanyName = $scope.EmpModel.CompanyName;
                e.ProductName = $scope.EmpModel.ProductName;
                e.Emailid = $scope.EmpModel.Emailid;
                e.CategoryName = $scope.EmpModel.CategoryName;
                e.DatepickerSelect = $scope.EmpModel.DatepickerSelect;
                console.log('e.CategoryName....',e.CategoryName);
            }

            localStorage.setItem("items", JSON.stringify($scope.EmpList));
        });

        $.notify("Update Data Successfully","success");
    };

    $scope.editData = function (product) {
        $scope.EmpModel.Id = product.Id;
        $scope.EmpModel.CompanyName = product.CompanyName;
        $scope.EmpModel.ProductName = product.ProductName;
        $scope.EmpModel.Emailid = product.Emailid;
        $scope.EmpModel.CategoryName = product.CategoryName;
        $scope.EmpModel.DatepickerSelect = product.DatepickerSelect;
        $('.add-btn').css('display','none');
        $('.update-btn').css('display','inline');
        $('.cnl-btn').css('display','inline');
        $('.add-form').css('display','none');
        $('.edit-form').css('display','block');
        localStorage.setItem("items", JSON.stringify($scope.EmpList));
        /*$location.url('editpage');*/
    };
    $scope.removeRow = function (d) {
        var cnf = confirm('Are you sure want to delete data ??');
        if(cnf === true){
            $scope.EmpList.splice(d,1);
        }else{
            return false;
        }
        $.notify("Delete data Successfully","error");
        localStorage.setItem("items", JSON.stringify($scope.EmpList));
    };
    $scope.clearModel = function () {
        $scope.EmpModel.Id = 0;
        $scope.EmpModel.CompanyName = '';
        $scope.EmpModel.ProductName = '';
        $scope.EmpModel.Emailid = '';
        $scope.EmpModel.CategoryName ='';
        $scope.EmpModel.DatepickerSelect ='';
        $('.add-btn').css('display','inline');
        $('.update-btn').css('display','none');
        $('.cnl-btn').css('display','none');
        $('.add-form').css('display','block');
        $('.edit-form').css('display','none');

    };

    $scope.css_form = {
        'border': '3px solid black',
        /*'padding': '17px 17px 8px 0px',*/
        'width': '65%',
        'text-align': '-webkit-left',
        'margin-left': '18%',
        'margin-top': '12px',
        'margin-bottom': '26px',
        'background-color': '#bcccff',
        'padding': '19px 21px 17px 12px',
        'border-radius': '13px'
    };
    $scope.css_form2 = {
        'width' :'100%',
        'text-align': '-webkit-left',
        'margin-left': '0%',
        'margin-bottom': '26px',
        'background-color': 'rgb(158, 181, 255)',
        'padding': '19px 21px 17px 12px'
    };

    $scope.css_form_table = {
        'width':'100%',
        'margin-top': '2%'
    };
    $scope.init = function () {
        $scope.EmpList =  localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];
    };
    //$scope.initfun();
   /* $scope.dataval = function() {
        $( "#datepicker" ).datepicker();
        var date = $("#datepicker").datepicker("getDate");
        alert($.datepicker.formatDate("dd-mm-yy", date));
    };*/


});
